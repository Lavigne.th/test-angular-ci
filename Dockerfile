#FROM nginx:stable-alpine
#COPY /dist/ng-unit-test /usr/share/nginx/html
#RUN apk update && apk add --no-cache wget
#RUN apk add --no-cache libuv \
#    && apk add --no-cache --update-cache  nodejs npm \
#    && echo "NodeJS Version:" "$(node -v)" \
#    && echo "NPM Version:" "$(npm -v)"
#RUN node -v
#RUN ls

FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

FROM nginx:stable-alpine
COPY --from=node /app/dist/ng-unit-test /usr/share/nginx/html