import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerDto } from '../dto/data/player-dto';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  public getPlayers(): Observable<Array<PlayerDto>> {
    return this.http.get<Array<PlayerDto>>('');
  }
}
