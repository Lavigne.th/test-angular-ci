import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PlayerDto } from 'src/app/dto/data/player-dto';

import { PlayerComponent } from './player.component';

describe('PlayerComponent', () => {
  let component: PlayerComponent;
  let fixture: ComponentFixture<PlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      
      declarations: [ PlayerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('set player data', () => {
    fixture.componentInstance.player = new PlayerDto('Jacky', 10);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('.old') !== undefined).toBeTrue();
    expect(fixture.nativeElement.querySelector('.old').innerText).toContain('Jacky')
    expect(fixture.debugElement.queryAll(By.css('.old')).length).toEqual(1);
  });
});
