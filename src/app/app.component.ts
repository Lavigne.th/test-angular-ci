import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PlayerDto } from './dto/data/player-dto';
import { TaskDto as ProductDto } from './dto/data/product-dto';
import { PlayerService } from './service/player.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'ng-unit-test';

  public players: Array<PlayerDto> = [];

  public isError: boolean = false;

  @ViewChild('usernameInput') input?: ElementRef; 

  @ViewChild('passwordInput') password?: ElementRef; 

  public products: Array<ProductDto> = [];

  constructor(private playerService: PlayerService, private http: HttpClient) {

  }

  ngOnInit(): void {
    this.playerService.getPlayers().subscribe((players: Array<PlayerDto>) => {
      this.players = players;
    });
  }

  onSubmit(): void {
    if(this.password?.nativeElement.value.length <= 4) {
      this.isError = true;
    } else {
      this.isError = false;
    }
  }

  onLoad(): void {
    this.http.get<Array<ProductDto>>('https://dummyjson.com/products').subscribe((products: any) => {
      this.products = products.products;
    });
  }
}
