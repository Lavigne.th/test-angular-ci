import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { PlayerDto } from 'src/app/dto/data/player-dto';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  @Input() player?: PlayerDto;

  constructor() { }

  ngOnInit(): void {
  }

}
