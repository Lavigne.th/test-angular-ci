describe('My First Test', () => {
  it('should fail if are not the right lenght', () => {
    cy.visit('/')
    cy.get('[data-cy-role="username"]').type('admin');
    cy.get('[data-cy-role="password"]').type('adm');
    cy.get('[data-cy-role="submit"]').click();
    
    cy.get('[data-cy-role="error"]').should('satisfy', (elem: any) => {
      return elem[0].hasAttribute('hidden') === false;
    });
  });

  it('should pass', () => {
    cy.visit('/')
    cy.get('[data-cy-role="username"]').type('admin');
    cy.get('[data-cy-role="password"]').type('admin');
    cy.get('[data-cy-role="submit"]').click();
    
    cy.get('[data-cy-role="error"]').should('satisfy', (elem: any) => {
      return elem[0].hasAttribute('hidden') === true;
    });
  });

  it('should load', () => {
    cy.intercept('https://dummyjson.com/products').as('products');
    cy.get('[data-cy-role="load"]').click();
    cy.wait('@products', { timeout: 50000 }).then(
      (interceptions) => {
        if(interceptions.response !== undefined && interceptions.response.body !== undefined) {
          return true;
        }
        return false;
      }
    )
  });
})
